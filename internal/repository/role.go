package repository

import (
	"br-user-service/configs"
	"br-user-service/internal/entity"
	"br-user-service/pkg/mongodb"
	"context"
	"errors"
	"fmt"
	"log"
	"os"
	"time"

	paginate "github.com/gobeam/mongo-go-pagination"
	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const RoleCollection = "roles"

type RoleRepository interface {
	Paginate(ctx context.Context, page int64, limit int64, filter bson.M) ([]entity.Role, *paginate.PaginationData, error)
	FindAll(ctx context.Context) ([]entity.Role, error)
	FindById(ctx context.Context, id string) (*entity.Role, error)
	Store(ctx context.Context, role entity.CreateRole) (string, error)
	UpdateById(ctx context.Context, id string, role entity.CreateRole) (*mongo.UpdateResult, error)
	Delete(ctx context.Context, id string) (*mongo.DeleteResult, error)
}

type roleRepository struct {
	c *mongo.Collection
}

func NewRoleRepository(conn mongodb.Connection, cfg *configs.Config) RoleRepository {
	collection := conn.DB(cfg).Collection(RoleCollection)
	models := []mongo.IndexModel{
		{
			Keys: bson.M{
				"uuid": 1,
			},
			Options: options.Index().SetUnique(true),
		},
		{
			Keys: bson.M{
				"role_name": 1,
			},
			Options: nil,
		},
	}
	result, err := collection.Indexes().CreateMany(context.Background(), models)
	if err != nil {
		log.Println("Failed to Create Role Name Index", err)
		os.Exit(1)
	}

	log.Println("Role Index [role_name]", result)

	return &roleRepository{collection}
}

func (r *roleRepository) Paginate(ctx context.Context, page int64, limit int64, filter bson.M) ([]entity.Role, *paginate.PaginationData, error) {
	var roles []entity.Role
	// Just Show Permissions ID without lookup for Pagination
	// lookup := bson.M{
	// 	"$lookup": bson.M{
	// 		"from":         "permissions",
	// 		"localField":   "permissions_id",
	// 		"foreignField": "_id",
	// 		"as":           "permissions",
	// 	},
	// }
	// projection := bson.M{
	// 	"$project": bson.M{
	// 		"permissions_id": 0,
	// 	},
	// }

	aggPaginatedData, err := paginate.New(r.c).Context(ctx).Limit(limit).Page(page).Aggregate(filter)
	if err != nil {
		return nil, nil, err
	}

	for _, raw := range aggPaginatedData.Data {
		var role *entity.Role
		if marshallErr := bson.Unmarshal(raw, &role); marshallErr == nil {
			roles = append(roles, *role)
		}

	}

	return roles, &aggPaginatedData.Pagination, err
}

func (r *roleRepository) FindAll(ctx context.Context) ([]entity.Role, error) {
	var roles []entity.Role
	query := []bson.M{
		{
			"$lookup": bson.M{
				"from":         "permissions",
				"localField":   "permissions_id",
				"foreignField": "uuid",
				"as":           "permissions",
			},
		},
		{
			"$project": bson.M{
				"permissions_id": 0,
			},
		},
	}

	cursor, err := r.c.Aggregate(ctx, query)
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)

	var role entity.Role
	for cursor.Next(ctx) {
		err := cursor.Decode(&role)
		if err != nil {
			return nil, err
		}

		log.Println("role", role)
		roles = append(roles, role)
	}

	if err := cursor.Err(); err != nil {
		return nil, err
	}

	return roles, nil
}

func (r *roleRepository) FindById(ctx context.Context, id string) (*entity.Role, error) {
	var role *entity.Role

	qry := []bson.M{
		{
			"$match": bson.M{
				"uuid": id,
			},
		},
		{
			"$lookup": bson.M{
				"from":         "permissions",
				"localField":   "permissions_id",
				"foreignField": "uuid",
				"as":           "permissions",
			},
		},
		{
			"$project": bson.M{
				"permissions_id": 0,
			},
		},
	}

	result, err := r.c.Aggregate(ctx, qry)
	if err != nil {
		return nil, err
	}

	for result.Next(ctx) {
		err := result.Decode(&role)
		if err != nil {
			log.Println("Failed to Decode Cursor to Rolw Struct", err)
		}
	}

	defer result.Close(ctx)

	if role == nil {
		return nil, mongo.ErrNoDocuments
	}

	return role, nil
}

func (r *roleRepository) Store(ctx context.Context, role entity.CreateRole) (string, error) {
	role.UUID = uuid.NewString()
	role.CreatedAt = time.Now()
	role.UpdatedAt = time.Now()

	count, err := r.c.CountDocuments(ctx, bson.M{"role_name": role.RoleName})
	if err != nil {
		return "", err
	}

	if count > 0 {
		duplicateErr := fmt.Sprintf("%s Already Exist!", role.RoleName)
		return "", errors.New(duplicateErr)
	}

	_, err = r.c.InsertOne(ctx, &role)
	if err != nil {
		return "", err
	}

	return role.UUID, nil
}

func (r *roleRepository) UpdateById(ctx context.Context, id string, role entity.CreateRole) (*mongo.UpdateResult, error) {
	return r.c.UpdateOne(ctx, bson.M{"uuid": id}, bson.M{"$set": bson.M{
		"role_name":      role.RoleName,
		"permissions_id": role.PermissionsID,
		"updated_at":     time.Now(),
	}})
}

func (r *roleRepository) Delete(ctx context.Context, id string) (*mongo.DeleteResult, error) {
	return r.c.DeleteOne(ctx, bson.M{"uuid": id})
}
