package repository

import (
	"br-user-service/configs"
	"br-user-service/internal/entity"
	"br-user-service/pkg/mongodb"
	"context"
	"errors"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const PermissionCollection = "permissions"

type PermissionRepository interface {
	FindAll(ctx context.Context) ([]entity.Permission, error)
	Store(ctx context.Context, permission entity.Permission) (string, error)
}

type permissionRepository struct {
	c *mongo.Collection
}

func NewPermissionRepository(conn mongodb.Connection, cfg *configs.Config) PermissionRepository {
	collection := conn.DB(cfg).Collection(PermissionCollection)
	models := []mongo.IndexModel{
		{
			Keys: bson.M{
				"uuid": 1,
			},
			Options: options.Index().SetUnique(true),
		},
		{
			Keys: bson.M{
				"name": 1,
			},
			Options: options.Index().SetUnique(true),
		},
	}
	result, err := collection.Indexes().CreateMany(context.Background(), models)
	if err != nil {
		log.Println("Failed to Create Permission Name Index", err)
		os.Exit(1)
	}

	log.Println("Permission Index [name]", result)

	return &permissionRepository{collection}
}

func (r *permissionRepository) FindAll(ctx context.Context) ([]entity.Permission, error) {
	var permissions []entity.Permission
	cursor, err := r.c.Find(ctx, bson.M{})
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		var permission entity.Permission
		err := cursor.Decode(&permission)
		if err != nil {
			return nil, err
		}

		permissions = append(permissions, permission)
	}

	if err := cursor.Err(); err != nil {
		return nil, err
	}

	return permissions, nil
}

func (r *permissionRepository) Store(ctx context.Context, permission entity.Permission) (string, error) {
	permission.UUID = uuid.NewString()
	permission.CreatedAt = time.Now()
	permission.UpdatedAt = time.Now()

	count, err := r.c.CountDocuments(ctx, bson.M{"name": permission.Name})
	if err != nil {
		return "", err
	}

	if count > 0 {
		duplicateErr := fmt.Sprintf("%s Already Exist!", permission.Name)
		return "", errors.New(duplicateErr)
	}

	_, err = r.c.InsertOne(ctx, &permission)
	if err != nil {
		return "", err
	}

	return permission.UUID, nil
}
