package repository

import (
	"br-user-service/configs"
	"br-user-service/internal/entity"
	"br-user-service/pkg/helper"
	"br-user-service/pkg/mongodb"
	"context"
	"errors"
	"fmt"
	"log"
	"os"
	"time"

	paginate "github.com/gobeam/mongo-go-pagination"
	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const UserAdminCollection = "users_admin"

type UserAdminRepository interface {
	Paginate(ctx context.Context, page int64, limit int64, filter bson.M) ([]entity.UserAdmin, *paginate.PaginationData, error)
	FindById(ctx context.Context, id string) (*entity.UserAdmin, error)
	Store(ctx context.Context, userAdmin entity.CreateUserAdmin) (string, error)
	UpdateById(ctx context.Context, id string, userAdmin entity.CreateUserAdmin) (*mongo.UpdateResult, error)
	Delete(ctx context.Context, id string) (*mongo.DeleteResult, error)
}

type userAdminRepository struct {
	c *mongo.Collection
}

func NewUserAdminRepository(conn mongodb.Connection, cfg *configs.Config) UserAdminRepository {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	collection := conn.DB(cfg).Collection(UserAdminCollection)
	models := []mongo.IndexModel{
		{
			Keys: bson.M{
				"uuid": 1,
			},
			Options: options.Index().SetUnique(true),
		},
		{
			Keys: bson.M{
				"email": 1,
			},
			Options: options.Index().SetUnique(true),
		},
		{
			Keys: bson.M{
				"first_name": 1,
			},
			Options: options.Index().SetUnique(false),
		},
		{
			Keys: bson.M{
				"last_name": 1,
			},
			Options: options.Index().SetUnique(false),
		},
	}
	result, err := collection.Indexes().CreateMany(ctx, models)
	if err != nil {
		log.Panicf("Failed to Create Indexes for %s Collections", UserAdminCollection)
		os.Exit(1)
	}

	log.Println("Indexes Created", result)

	return &userAdminRepository{collection}
}

func (r *userAdminRepository) Paginate(ctx context.Context, page int64, limit int64, filter bson.M) ([]entity.UserAdmin, *paginate.PaginationData, error) {
	var userAdmins []entity.UserAdmin
	lookup := bson.M{
		"$lookup": bson.M{
			"from":         "roles",
			"localField":   "role_id",
			"foreignField": "uuid",
			"as":           "role",
		},
	}

	setRole := bson.M{
		"$set": bson.M{
			"role": bson.M{
				"$first": "$role",
			},
		},
	}

	setRoleTopLevel := bson.M{
		"$set": bson.M{
			"role_name":   "$role.role_name",
			"permissions": "$role.permissions",
		},
	}

	hidePassword := bson.M{
		"$unset": bson.A{
			"password",
		},
	}

	aggPaginatedData, err := paginate.New(r.c).Context(ctx).Limit(limit).Page(page).Aggregate(filter, lookup, setRole, setRoleTopLevel, hidePassword)
	if err != nil {
		return nil, nil, err
	}

	for _, raw := range aggPaginatedData.Data {
		var userAdmin *entity.UserAdmin
		if marshallErr := bson.Unmarshal(raw, &userAdmin); marshallErr == nil {
			userAdmins = append(userAdmins, *userAdmin)
		}

	}

	return userAdmins, &aggPaginatedData.Pagination, err
}

func (r *userAdminRepository) FindById(ctx context.Context, id string) (*entity.UserAdmin, error) {
	var userAdmin *entity.UserAdmin

	qry := []bson.M{
		{
			"$match": bson.M{
				"uuid": id,
			},
		},
		{
			"$lookup": bson.M{
				"from":         "roles",
				"localField":   "role_id",
				"foreignField": "uuid",
				"as":           "role",
			},
		},
		{
			"$set": bson.M{
				"role": bson.M{
					"$first": "$role",
				},
			},
		},
		{
			"$lookup": bson.M{
				"from":         "permissions",
				"localField":   "role.permissions_id",
				"foreignField": "uuid",
				"as":           "role.permissions",
			},
		},
		{
			"$set": bson.M{
				"role_name":   "$role.role_name",
				"permissions": "$role.permissions",
			},
		},
		{
			"$unset": bson.A{
				"password",
			},
		},
	}

	result, err := r.c.Aggregate(ctx, qry)
	if err != nil {
		return nil, err
	}

	for result.Next(ctx) {
		err := result.Decode(&userAdmin)
		if err != nil {
			log.Println("Failed to Decode Cursor to Rolw Struct", err)
		}
	}

	defer result.Close(ctx)

	if userAdmin == nil {
		return nil, mongo.ErrNoDocuments
	}

	return userAdmin, nil
}

func (r *userAdminRepository) Store(ctx context.Context, userAdmin entity.CreateUserAdmin) (string, error) {
	userAdmin.UUID = uuid.NewString()
	userAdmin.Password = helper.HashAndSalt([]byte(userAdmin.Password))
	userAdmin.CreatedAt = time.Now()
	userAdmin.UpdatedAt = time.Now()
	userAdmin.IsEmailVerified = false

	count, err := r.c.CountDocuments(ctx, bson.M{"email": userAdmin.Email})
	if err != nil {
		return "", err
	}

	if count > 0 {
		duplicateErr := fmt.Sprintf("%s Already Exist!", userAdmin.Email)
		return "", errors.New(duplicateErr)
	}

	_, err = r.c.InsertOne(ctx, &userAdmin)
	if err != nil {
		return "", err
	}

	return userAdmin.UUID, nil
}

func (r *userAdminRepository) UpdateById(ctx context.Context, id string, userAdmin entity.CreateUserAdmin) (*mongo.UpdateResult, error) {
	// This Repository only Changing Basic info (email and password need different Function)
	return r.c.UpdateOne(ctx, bson.M{"uuid": id}, bson.M{"$set": bson.M{
		"first_name": userAdmin.FirstName,
		"last_name":  userAdmin.LastName,
		"role_id":    userAdmin.RoleID,
		"updated_at": time.Now(),
	}})
}

func (r *userAdminRepository) Delete(ctx context.Context, id string) (*mongo.DeleteResult, error) {
	return r.c.DeleteOne(ctx, bson.M{"uuid": id})
}
