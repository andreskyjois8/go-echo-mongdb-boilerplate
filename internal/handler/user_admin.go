package handler

import (
	"br-user-service/internal/entity"
	"br-user-service/internal/repository"
	"br-user-service/pkg/helper"
	"context"
	"fmt"
	"strconv"
	"time"

	"github.com/labstack/echo/v4"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type UserAdminHandler interface {
	Paginate(c echo.Context) error
	FindUserAdminById(c echo.Context) error
	CreateUserAdmin(c echo.Context) error
	UpdateUserAdmin(c echo.Context) error
	DeleteUserAdmin(c echo.Context) error
}

type userAdminHandler struct {
	userAdminRepo repository.UserAdminRepository
	timeout       time.Duration
}

func NewUserAdminHandler(userAdminRepo repository.UserAdminRepository) UserAdminHandler {
	return &userAdminHandler{userAdminRepo, 10 * time.Second}
}

func (h *userAdminHandler) Paginate(c echo.Context) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	pageParam := c.QueryParam("page")
	if pageParam == "" {
		pageParam = "1"
	}
	page, _ := strconv.Atoi(pageParam)

	limitParam := c.QueryParam("limit")
	if limitParam == "" {
		limitParam = "15"
	}
	limit, _ := strconv.Atoi(limitParam)

	// Filter Must in aggregate pipeline
	filter := bson.M{
		"$match": bson.M{
			"email": bson.M{"$regex": c.QueryParam("search"), "$options": "i"},
		},
	}

	userAdmins, pagination, err := h.userAdminRepo.Paginate(ctx, int64(page), int64(limit), filter)
	if err != nil {
		return helper.BadRequestResponse(c, helper.Response{
			Message: "Failed to Get Human List",
			Errors:  err.Error(),
		})
	}

	return helper.OkResponse(c, helper.Response{
		Data:       userAdmins,
		Pagination: pagination,
	})
}

func (h *userAdminHandler) FindUserAdminById(c echo.Context) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	paramId := c.Param("id")

	if paramId == "" {
		return helper.BadRequestResponse(c, helper.Response{
			Message: "Please Provide Valid UserAdmin ID!",
		})
	}

	userAdmin, err := h.userAdminRepo.FindById(ctx, paramId)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return helper.NotFoundResponse(c, helper.Response{
				Message: fmt.Sprintf("UserAdmin With [ID: %s] Not Found", paramId),
			})
		}

		return helper.BadRequestResponse(c, helper.Response{
			Message: fmt.Sprintf("Failed to Get UserAdmin With [ID: %s]", paramId),
			Errors:  err.Error(),
		})
	}

	return helper.OkResponse(c, helper.Response{
		Data: userAdmin,
	})
}

func (h *userAdminHandler) CreateUserAdmin(c echo.Context) error {
	ctx, cancel := context.WithTimeout(context.Background(), h.timeout)
	defer cancel()

	var userAdmin entity.CreateUserAdmin
	if err := c.Bind(&userAdmin); err != nil {
		return helper.BadRequestResponse(c, helper.Response{
			Message: "Failed to Get Value from Body",
			Errors:  err.Error(),
		})
	}

	result, err := h.userAdminRepo.Store(ctx, userAdmin)
	if err != nil {
		return helper.UnprocResponse(c, helper.Response{
			Message: "Failed to Save UserAdmin!",
			Errors:  err.Error(),
		})
	}

	return helper.CreatedResponse(c, helper.Response{
		Message: "Role Saved!",
		Data:    result,
	})
}

func (h *userAdminHandler) UpdateUserAdmin(c echo.Context) error {
	ctx, cancel := context.WithTimeout(context.Background(), h.timeout)
	defer cancel()

	var userAdmin entity.CreateUserAdmin
	paramId := c.Param("id")
	if paramId == "" {
		return helper.BadRequestResponse(c, helper.Response{
			Message: "Please Provide Valid UserAdmin ID!",
		})
	}

	if err := c.Bind(&userAdmin); err != nil {
		return helper.BadRequestResponse(c, helper.Response{
			Message: "Failed to Get Value from Body",
			Errors:  err.Error(),
		})
	}

	result, err := h.userAdminRepo.UpdateById(ctx, paramId, userAdmin)
	if err != nil {
		return helper.UnprocResponse(c, helper.Response{
			Message: fmt.Sprintf("Failed to Update UserAdmin With [ID: %s]", paramId),
			Errors:  err.Error(),
		})
	}

	if result.MatchedCount == 0 {
		return helper.NotFoundResponse(c, helper.Response{
			Message: fmt.Sprintf("UserAdmin ID [ID: %s] Not Found", paramId),
		})
	}

	return helper.OkResponse(c, helper.Response{
		Message: "UserAdmin Updated!",
	})
}

func (h *userAdminHandler) DeleteUserAdmin(c echo.Context) error {
	ctx, cancel := context.WithTimeout(context.Background(), h.timeout)
	defer cancel()

	paramId := c.Param("id")
	if paramId == "" {
		return helper.BadRequestResponse(c, helper.Response{
			Message: "Please Provide Valid UserAdmin ID!",
		})
	}

	result, err := h.userAdminRepo.Delete(ctx, paramId)
	if err != nil {
		return helper.UnprocResponse(c, helper.Response{
			Message: fmt.Sprintf("Failed to Delete UserAdmin With [ID: %s]", paramId),
			Errors:  err.Error(),
		})
	}

	if result.DeletedCount == 0 {
		return helper.NotFoundResponse(c, helper.Response{
			Message: fmt.Sprintf("UserAdmin With [ID: %s] Not Found", paramId),
		})
	}

	return helper.OkResponse(c, helper.Response{
		Message: "UserAdmin Deleted!",
	})
}
