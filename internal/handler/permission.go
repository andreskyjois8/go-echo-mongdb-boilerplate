package handler

import (
	"br-user-service/internal/entity"
	"br-user-service/internal/repository"
	"br-user-service/pkg/helper"
	"context"
	"time"

	"github.com/labstack/echo/v4"
)

type PermissionHandler interface {
	GetAllPermission(c echo.Context) error
	CreatePermission(c echo.Context) error
}

type permissionHandler struct {
	permissionRepo repository.PermissionRepository
	timeout        time.Duration
}

func NewPermissionHandler(permissionRepo repository.PermissionRepository) PermissionHandler {
	return &permissionHandler{permissionRepo, 10 * time.Second}
}

func (h *permissionHandler) GetAllPermission(c echo.Context) error {
	ctx, cancel := context.WithTimeout(context.Background(), h.timeout)
	defer cancel()

	permissions, err := h.permissionRepo.FindAll(ctx)
	if err != nil {
		return helper.BadRequestResponse(c, helper.Response{
			Message: "Failed to Get Permission List",
			Errors:  err.Error(),
		})
	}

	return helper.OkResponse(c, helper.Response{
		Data: permissions,
	})
}

func (h *permissionHandler) CreatePermission(c echo.Context) error {
	ctx, cancel := context.WithTimeout(context.Background(), h.timeout)
	defer cancel()

	var permission entity.Permission
	if err := c.Bind(&permission); err != nil {
		return helper.BadRequestResponse(c, helper.Response{
			Message: "Failed to Get Value from Body",
			Errors:  err.Error(),
		})
	}

	result, err := h.permissionRepo.Store(ctx, permission)
	if err != nil {
		return helper.UnprocResponse(c, helper.Response{
			Message: "Failed to Save Permission!",
			Errors:  err.Error(),
		})
	}

	return helper.CreatedResponse(c, helper.Response{
		Message: "Permission Saved!",
		Data:    result,
	})
}
