package handler

import (
	"br-user-service/internal/entity"
	"br-user-service/internal/repository"
	"br-user-service/pkg/helper"
	"context"
	"fmt"
	"strconv"
	"time"

	"github.com/labstack/echo/v4"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type RoleHandler interface {
	Paginate(c echo.Context) error
	GetAllRole(c echo.Context) error
	FindRoleById(c echo.Context) error
	CreateRole(c echo.Context) error
	UpdateRole(c echo.Context) error
	DeleteRole(c echo.Context) error
}

type roleHandler struct {
	roleRepo repository.RoleRepository
	timeout  time.Duration
}

func NewRoleHandler(roleRepo repository.RoleRepository) RoleHandler {
	return &roleHandler{roleRepo, 10 * time.Second}
}

func (h *roleHandler) Paginate(c echo.Context) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	pageParam := c.QueryParam("page")
	if pageParam == "" {
		pageParam = "1"
	}
	page, _ := strconv.Atoi(pageParam)

	limitParam := c.QueryParam("limit")
	if limitParam == "" {
		limitParam = "15"
	}
	limit, _ := strconv.Atoi(limitParam)

	// Filter Must in aggregate pipeline
	filter := bson.M{
		"$match": bson.M{
			"role_name": bson.M{"$regex": c.QueryParam("search"), "$options": "i"},
		},
	}

	roles, pagination, err := h.roleRepo.Paginate(ctx, int64(page), int64(limit), filter)
	if err != nil {
		return helper.BadRequestResponse(c, helper.Response{
			Message: "Failed to Get Human List",
			Errors:  err.Error(),
		})
	}

	return helper.OkResponse(c, helper.Response{
		Data:       roles,
		Pagination: pagination,
	})
}

func (h *roleHandler) GetAllRole(c echo.Context) error {
	ctx, cancel := context.WithTimeout(context.Background(), h.timeout)
	defer cancel()

	roles, err := h.roleRepo.FindAll(ctx)
	if err != nil {
		return helper.BadRequestResponse(c, helper.Response{
			Message: "Failed to Get Role List",
			Errors:  err.Error(),
		})
	}

	return helper.OkResponse(c, helper.Response{
		Data: roles,
	})
}

func (h *roleHandler) FindRoleById(c echo.Context) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	paramId := c.Param("id")

	if paramId == "" {
		return helper.BadRequestResponse(c, helper.Response{
			Message: "Please Provide Valid Role ID!",
		})
	}

	role, err := h.roleRepo.FindById(ctx, paramId)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return helper.NotFoundResponse(c, helper.Response{
				Message: fmt.Sprintf("Role With [ID: %s] Not Found", paramId),
			})
		}

		return helper.BadRequestResponse(c, helper.Response{
			Message: fmt.Sprintf("Failed to Get Role With [ID: %s]", paramId),
			Errors:  err.Error(),
		})
	}

	return helper.OkResponse(c, helper.Response{
		Data: role,
	})
}

func (h *roleHandler) CreateRole(c echo.Context) error {
	ctx, cancel := context.WithTimeout(context.Background(), h.timeout)
	defer cancel()

	var role entity.CreateRole
	if err := c.Bind(&role); err != nil {
		return helper.BadRequestResponse(c, helper.Response{
			Message: "Failed to Get Value from Body",
			Errors:  err.Error(),
		})
	}

	result, err := h.roleRepo.Store(ctx, role)
	if err != nil {
		return helper.UnprocResponse(c, helper.Response{
			Message: "Failed to Save Role!",
			Errors:  err.Error(),
		})
	}

	return helper.CreatedResponse(c, helper.Response{
		Message: "Role Saved!",
		Data:    result,
	})
}

func (h *roleHandler) UpdateRole(c echo.Context) error {
	ctx, cancel := context.WithTimeout(context.Background(), h.timeout)
	defer cancel()

	var role entity.CreateRole
	paramId := c.Param("id")
	if paramId == "" {
		return helper.BadRequestResponse(c, helper.Response{
			Message: "Please Provide Valid Role ID!",
		})
	}

	if err := c.Bind(&role); err != nil {
		return helper.BadRequestResponse(c, helper.Response{
			Message: "Failed to Get Value from Body",
			Errors:  err.Error(),
		})
	}

	result, err := h.roleRepo.UpdateById(ctx, paramId, role)
	if err != nil {
		return helper.UnprocResponse(c, helper.Response{
			Message: fmt.Sprintf("Failed to Update Role With [ID: %s]", paramId),
			Errors:  err.Error(),
		})
	}

	if result.MatchedCount == 0 {
		return helper.NotFoundResponse(c, helper.Response{
			Message: fmt.Sprintf("Role ID [ID: %s] Not Found", paramId),
		})
	}

	return helper.OkResponse(c, helper.Response{
		Message: "Role Updated!",
	})
}

func (h *roleHandler) DeleteRole(c echo.Context) error {
	ctx, cancel := context.WithTimeout(context.Background(), h.timeout)
	defer cancel()

	paramId := c.Param("id")
	if paramId == "" {
		return helper.BadRequestResponse(c, helper.Response{
			Message: "Please Provide Valid Role ID!",
		})
	}

	result, err := h.roleRepo.Delete(ctx, paramId)
	if err != nil {
		return helper.UnprocResponse(c, helper.Response{
			Message: fmt.Sprintf("Failed to Delete Role With [ID: %s]", paramId),
			Errors:  err.Error(),
		})
	}

	if result.DeletedCount == 0 {
		return helper.NotFoundResponse(c, helper.Response{
			Message: fmt.Sprintf("Role With [ID: %s] Not Found", paramId),
		})
	}

	return helper.OkResponse(c, helper.Response{
		Message: "Role Deleted!",
	})
}
