package entity

import (
	"time"
)

type Role struct {
	UUID          string       `json:"uuid,omitempty" bson:"uuid"`
	RoleName      string       `json:"role_name,omitempty" bson:"role_name"`
	PermissionsID []string     `json:"permissions_id,omitempty" bson:"permissions_id"`
	Permission    []Permission `json:"permissions,omitempty" bson:"permissions,omitempty"`
	CreatedAt     time.Time    `bson:"created_at,omitempty" json:"created_at"`
	UpdatedAt     time.Time    `bson:"updated_at,omitempty" json:"updated_at"`
}

type CreateRole struct {
	UUID          string    `json:"uuid,omitempty" bson:"uuid"`
	RoleName      string    `json:"role_name,omitempty" bson:"role_name"`
	PermissionsID []string  `json:"permissions_id,omitempty" bson:"permissions_id"`
	CreatedAt     time.Time `bson:"created_at,omitempty" json:"created_at"`
	UpdatedAt     time.Time `bson:"updated_at,omitempty" json:"updated_at"`
}

type Permission struct {
	UUID      string    `json:"uuid,omitempty" bson:"uuid"`
	Name      string    `json:"name,omitempty" bson:"name"`
	CreatedAt time.Time `bson:"created_at,omitempty" json:"created_at"`
	UpdatedAt time.Time `bson:"updated_at,omitempty" json:"updated_at"`
}
