package entity

import (
	"time"
)

type UserAdmin struct {
	UUID      string `json:"uuid,omitempty" bson:"uuid"`
	Email     string `json:"email,omitempty" bson:"email"`
	FirstName string `json:"first_name,omitempty" bson:"first_name"`
	LastName  string `json:"last_name,omitempty" bson:"last_name"`
	Password  string `json:"password,omitempty" bson:"password"`
	RoleID    string `json:"role_id,omitempty" bson:"role_id"`
	// Role        Role               `json:"role,omitempty" bson:"role,omitempty"`
	RoleName        string       `json:"role_name,omitempty" bson:"role_name,omitempty"`
	Permissions     []Permission `json:"permissions,omitempty" bson:"permissions,omitempty"`
	IsEmailVerified bool         `json:"email_verified" bson:"email_verified"`
	CreatedAt       time.Time    `bson:"created_at,omitempty" json:"created_at"`
	UpdatedAt       time.Time    `bson:"updated_at,omitempty" json:"updated_at"`
}

type CreateUserAdmin struct {
	UUID            string    `json:"uuid,omitempty" bson:"uuid"`
	Email           string    `json:"email,omitempty" bson:"email"`
	FirstName       string    `json:"first_name,omitempty" bson:"first_name"`
	LastName        string    `json:"last_name,omitempty" bson:"last_name"`
	Password        string    `json:"password,omitempty" bson:"password"`
	RoleID          string    `json:"role_id,omitempty" bson:"role_id"`
	IsEmailVerified bool      `json:"email_verified" bson:"email_verified"`
	CreatedAt       time.Time `bson:"created_at,omitempty" json:"created_at"`
	UpdatedAt       time.Time `bson:"updated_at,omitempty" json:"updated_at"`
}
