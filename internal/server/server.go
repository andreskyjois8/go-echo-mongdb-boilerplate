package server

import (
	"br-user-service/configs"
	"br-user-service/internal/injector"
	"br-user-service/pkg/mongodb"
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type Server interface {
	Run() error
}

type server struct {
	conn mongodb.Connection
	cfg  *configs.Config
}

func NewServer(conn mongodb.Connection, cfg *configs.Config) Server {
	return &server{conn, cfg}
}

func (srv *server) Run() error {
	port := srv.cfg.App.Port

	server := echo.New()
	server.Use(middleware.Logger())
	server.Use(middleware.Recover())
	server.Use((middleware.CORSWithConfig(
		middleware.CORSConfig{
			AllowOrigins: []string{"*"},
			AllowMethods: []string{http.MethodGet, http.MethodPost, http.MethodPut, http.MethodDelete, http.MethodOptions, http.MethodPatch},
			AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
		},
	)))

	server.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Welcome to MS-GB User Service")
	})

	server.Any("/*", func(c echo.Context) error {
		return c.JSON(http.StatusNotFound, map[string]interface{}{
			"success": false,
			"code":    http.StatusNotFound,
			"message": "Route is Not Exist",
		})
	})

	injection := injector.NewInjector(srv.conn, server, srv.cfg)
	injection.InjectModules()

	log.Println(fmt.Printf("%v is Running at port %v ... ", srv.cfg.App.Name, port))

	// Start Server
	go func() {
		if err := server.Start(":" + port); err != nil && err != http.ErrServerClosed {
			server.Logger.Fatal("shutting down the Polaris GBMarket SERVICE ENDPOINT!")
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with a timeout of 10 seconds.
	// Use a buffered channel to avoid missing signals as recommended for signal.Notify
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		server.Logger.Fatal(err)
		return err
	}

	return nil
}
