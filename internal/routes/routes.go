package routes

import (
	"br-user-service/internal/middleware"

	"github.com/labstack/echo/v4"
)

type Routes interface {
	Install(server *echo.Echo, authMw middleware.AuthMiddleware)
}
