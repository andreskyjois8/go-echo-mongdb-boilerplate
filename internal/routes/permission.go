package routes

import (
	"br-user-service/internal/handler"
	"br-user-service/internal/middleware"

	"github.com/labstack/echo/v4"
)

type permissionRoutes struct {
	permissionHandler handler.PermissionHandler
}

func NewPermissionRoutes(permissionHandler handler.PermissionHandler) Routes {
	return &permissionRoutes{permissionHandler}
}

func (r *permissionRoutes) Install(server *echo.Echo, authMw middleware.AuthMiddleware) {
	v1 := server.Group("/api/v1/permissions")
	v1.GET("", r.permissionHandler.GetAllPermission)
	v1.POST("", r.permissionHandler.CreatePermission)
}
