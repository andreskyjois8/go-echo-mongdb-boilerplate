package routes

import (
	"br-user-service/internal/handler"
	"br-user-service/internal/middleware"

	"github.com/labstack/echo/v4"
)

type userAdminRoutes struct {
	userAdminHandler handler.UserAdminHandler
}

func NewUserAdminRoutes(userAdminHandler handler.UserAdminHandler) Routes {
	return &userAdminRoutes{userAdminHandler}
}

func (r *userAdminRoutes) Install(server *echo.Echo, authMw middleware.AuthMiddleware) {
	v1 := server.Group("/api/v1/users/admin")
	v1.GET("", r.userAdminHandler.Paginate)
	v1.GET("/:id", r.userAdminHandler.FindUserAdminById)
	v1.POST("", r.userAdminHandler.CreateUserAdmin)
	v1.PUT("/:id", r.userAdminHandler.UpdateUserAdmin)
	v1.DELETE("/:id", r.userAdminHandler.DeleteUserAdmin)
}
