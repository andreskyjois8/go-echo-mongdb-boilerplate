package routes

import (
	"br-user-service/internal/handler"
	"br-user-service/internal/middleware"

	"github.com/labstack/echo/v4"
)

type roleRoutes struct {
	roleHandler handler.RoleHandler
}

func NewRoleRoutes(roleHandler handler.RoleHandler) Routes {
	return &roleRoutes{roleHandler}
}

func (r *roleRoutes) Install(server *echo.Echo, authMw middleware.AuthMiddleware) {
	v1 := server.Group("/api/v1/roles")
	v1.GET("", r.roleHandler.Paginate)
	v1.GET("/all", r.roleHandler.GetAllRole)
	v1.GET("/:id", r.roleHandler.FindRoleById)
	v1.POST("", r.roleHandler.CreateRole)
	v1.PUT("/:id", r.roleHandler.UpdateRole)
	v1.DELETE("/:id", r.roleHandler.DeleteRole)
}
