package injector

import (
	"br-user-service/configs"
	"br-user-service/internal/handler"
	"br-user-service/internal/middleware"
	"br-user-service/internal/repository"
	"br-user-service/internal/routes"
	"br-user-service/pkg/mongodb"

	"github.com/labstack/echo/v4"
)

type Injector interface {
	InjectModules()
}

type injector struct {
	conn   mongodb.Connection
	server *echo.Echo
	cfg    *configs.Config
}

func NewInjector(conn mongodb.Connection, server *echo.Echo, cfg *configs.Config) Injector {
	return &injector{conn, server, cfg}
}

func (i *injector) InjectModules() {
	authMw := middleware.NewAuthMiddleware()

	permissionRepo := repository.NewPermissionRepository(i.conn, i.cfg)
	permissionHandler := handler.NewPermissionHandler(permissionRepo)
	permissionRoutes := routes.NewPermissionRoutes(permissionHandler)

	roleRepo := repository.NewRoleRepository(i.conn, i.cfg)
	roleHandler := handler.NewRoleHandler(roleRepo)
	roleRoutes := routes.NewRoleRoutes(roleHandler)

	userAdminRepo := repository.NewUserAdminRepository(i.conn, i.cfg)
	userAdminHandler := handler.NewUserAdminHandler(userAdminRepo)
	userAdminRoutes := routes.NewUserAdminRoutes(userAdminHandler)

	permissionRoutes.Install(i.server, authMw)
	roleRoutes.Install(i.server, authMw)
	userAdminRoutes.Install(i.server, authMw)
}
