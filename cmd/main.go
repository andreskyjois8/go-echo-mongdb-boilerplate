package main

import (
	"log"
	"os"

	"br-user-service/configs"
	"br-user-service/internal/server"
	"br-user-service/pkg/mongodb"
)

func main() {
	configPath := configs.GetConfigPath(os.Getenv("config"))
	cfg, err := configs.GetConfig(configPath)
	if err != nil {
		log.Fatalf("Loading config: %v", err)
	}

	conn := mongodb.NewConnection(cfg)
	defer conn.Close()

	server := server.NewServer(conn, cfg)

	log.Fatalln(server.Run())
}
