package mongodb

import (
	"br-user-service/configs"
	"context"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var ctx = context.TODO()

type Connection interface {
	Close()
	DB(cfg *configs.Config) *mongo.Database
}

type conn struct {
	client *mongo.Client
}

func NewConnection(cfg *configs.Config) Connection {
	var c conn
	var err error
	var clientOptions *options.ClientOptions
	url := getURL(cfg.MongoDB.Host, cfg.MongoDB.DbName)

	creds := options.Credential{
		Username: cfg.MongoDB.User,
		Password: cfg.MongoDB.Password,
	}

	clientOptions = options.Client().ApplyURI(url).SetAuth(creds)
	c.client, err = mongo.Connect(ctx, clientOptions)
	if err != nil {
		log.Panicln("Error to Connect Database", err)
		// os.Exit(1)
	}

	err = c.client.Ping(ctx, nil)
	if err != nil {
		log.Panicln("Error to Ping Database", err)
		// os.Exit(1)
	}

	log.Println("Connected to Database!")

	return &c
}

func (c *conn) Close() {
	c.client.Disconnect(ctx)
	log.Println("Disconnecting Database!")
}

func (c *conn) DB(cfg *configs.Config) *mongo.Database {
	return c.client.Database(cfg.MongoDB.DbName)
}

func getURL(host, dbName string) string {
	return fmt.Sprintf("mongodb+srv://%s/%s?retryWrites=true&w=majority",
		host,
		dbName,
	)
}
